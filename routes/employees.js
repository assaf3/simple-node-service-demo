const express = require('express');
const { v1 } = require('uuid');
const router = express.Router();

/* GET users listing. */
router.get('/', (req, res) => {
    const {
        db: { employees },
    } = req;
    employees.find({}, (err, data) => {
        if (err) {
            return res.sendStatus(500).send(err);
        }
        res.send(data);
    });
});

router.post('/', (req, res) => {
    const {
        db: { employees },
        body: { name, groupName, startDate },
    } = req;
    const id = v1();
    employees.insert(
        {
            _id: id,
            name,
            groupName,
            startDate,
        },
        (err) => {
            if (err) {
                return res.sendStatus(500).send(err);
            }
            res.send({ id });
        }
    );
});

router.put('/:id', (req, res) => {
    const {
        db: { employees },
        params: { id}, 
        body: { name, groupName, startDate },
    } = req;
    employees.update(
        {_id: id},
        {
            _id: id,
            name,
            groupName,
            startDate,
        },
        (err) => {
            if (err) {
                return res.sendStatus(500).send(err);
            }
            res.send({ id });
        }
    );
});

router.get('/:id', (req, res) => {
    const {
        db: { employees },
        params: { id },
    } = req;
    employees.findOne({ _id: id }, (err, data) => {
        if (err) {
            res.sendStatus(500).send(err);
        }
        if (!data) {
            return res.sendStatus(404);
        }
        res.send(data);
    });
});

router.delete('/:id', (req, res) => {
    const {
        db: { employees },
        params: { id },
    } = req;
    employees.remove({ _id: id }, (err) => {
        if (err) {
            return res.sendStatus(500).send(err);
        }
        res.send(`removed ${id}`);
    });
});

module.exports = router;
