const request = require('supertest');
const app = require('../app');

describe('Test App', () => {
    it('should test health endpoint', (done) => {
        request(app)
            .get('/health')
            .expect('Content-Type', /json/)
            .expect('Content-Length', '15')
            .expect(200)
            .end(function (err, res) {
                if (err) throw err;
                expect(res.text).toEqual('{"status":"ok"}');
                done();
            });
    });

    it('should test ready endpoint', () => {
        request(app)
            .get('/ready')
            .expect('Content-Type', /json/)
            .expect('Content-Length', '15')
            .expect(200)
            .end(function (err, res) {
                if (err) throw err;
                expect(res.text).toEqual('{"status":"ok"}');
            });
    });
});
