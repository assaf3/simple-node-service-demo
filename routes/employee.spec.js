const request = require('supertest');
const app = require('../app');

describe('Test App', () => {
    let employeeId;
    it('should test add employee endpoint', (done) => {
        request(app)
            .post('/employees')
            .send({ name: 'assaf', groupId: 'ServerSide', startDate: '2012-08-01' })
            .set('Accept', 'application/json')
            .expect(200)
            .end(function (err, res) {
                if (err) throw err;
                const response = JSON.parse(res.text);
                expect(response.id).toBeTruthy();
                employeeId = response.id;

                done();
            });
    });

    it('should test update employee endpoint', (done) => {
        request(app)
            .put(`/employees/${employeeId}`)
            .send({ name: 'john', groupId: 'ServerSide', startDate: '2012-08-01' })
            .set('Accept', 'application/json')
            .expect(200)
            .end(function (err, res) {
                if (err) throw err;
                done();
            });
    });

    it('should get an employee by ID', (done) => {
        request(app)
        .get(`/employees/${employeeId}`)
        .expect(200)
        .end(function (err, res) {
            if (err) throw err;
            const response = JSON.parse(res.text);
            expect(response.name).toEqual('john');
            done();
        });
    });

    it('should get all employees', (done) => {
        request(app)
        .get(`/employees`)
        .expect(200)
        .end(function (err, res) {
            if (err) throw err;
            const response = JSON.parse(res.text);
            expect(response.length).toBeTruthy();
            done();
        });
    });
    it('should test remove employee endpoint', (done) => {
        request(app)
            .delete(`/employees/${employeeId}`)
            .expect(200)
            .end(function (err, res) {
                if (err) throw err;
                done();
            });
    });
});
