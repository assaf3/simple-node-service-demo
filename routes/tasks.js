const express = require('express');
const { v1 } = require('uuid');
const router = express.Router();

router.post('/', (req, res) => {
    const {
        db: { tasks },
        body: { employeeId, customerId, startDate },
    } = req;
    const id = v1();
    tasks.insert(
        {
            _id: id,
            employeeId,
            customerId,
            startDate,
        },
        (err) => {
            if (err) {
                return res.sendStatus(500).send(err);
            }
            res.send({ id });
        }
    );
});

router.put('/:id', (req, res) => {
    const {
        db: { tasks },
        params: { id },
        body: { employeeId, customerId, startDate },
    } = req;
    tasks.update(
        { _id: id },
        {
            _id: id,
            employeeId,
            customerId,
            startDate,
        },
        (err) => {
            if (err) {
                return res.sendStatus(500).send(err);
            }
            res.send({ id });
        }
    );
});

router.get('/:id', (req, res) => {
    const {
        db: { tasks },
        params: { id },
    } = req;
    tasks.findOne({ _id: id }, (err, data) => {
        if (err) {
            res.sendStatus(500).send(err);
        }
        if (!data) {
            return res.sendStatus(404);
        }
        res.send(data);
    });
});

router.delete('/:id', (req, res) => {
    const {
        db: { tasks },
        params: { id },
    } = req;
    tasks.remove({ _id: id }, (err) => {
        if (err) {
            return res.sendStatus(500).send(err);
        }
        res.send(`removed ${id}`);
    });
});

module.exports = router;
