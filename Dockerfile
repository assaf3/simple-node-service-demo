FROM node:14-alpine

WORKDIR /src/app

COPY package*.json ./

# install node packages
RUN npm install

COPY . ./

RUN npm run build -- server

# define CMD
CMD [ "node", "./dist/apps/server/main" ]
