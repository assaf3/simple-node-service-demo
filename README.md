# Service 1 - express built in generator

 ```sh
$ npx express-generator --no-view --git users-service
$ cd users-service
$ npm i
$ npm start

```

## Adding stack dependencies

```sh
npm i nedb # Inmemmory mongo-like DB
npm i uuid # Guid generator
npm i -D jest # Test runner
```
