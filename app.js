const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const Datastore = require('nedb');

const db = {};
db.employees = new Datastore('.data/employees.db');
db.tasks = new Datastore('.data/tasks.db');

// You need to load each database (here we do it asynchronously)
db.employees.loadDatabase();
db.tasks.loadDatabase();

const indexRouter = require('./routes/index');
const employeesRouter = require('./routes/employees');
const tasksRouter = require('./routes/tasks');

const app = express();

app.use(logger('dev'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use((req, res, next) => {
    req.db = db;
    next();
})

app.use('/', indexRouter);
app.use('/employees', employeesRouter);
app.use('/tasks', tasksRouter);

module.exports = app;
